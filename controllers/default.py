# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

import os
import re
rule_pat = re.compile("^{.*",re.IGNORECASE)
num_rules_pat=re.compile("^Number of MER.*",re.IGNORECASE)
header_pat = re.compile("^Algo.*",re.IGNORECASE)
summary_pat = re.compile("^MER.*",re.IGNORECASE)
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simple replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Let us mine some mutual exclusion rules")
    form = SQLFORM(db.datasets,fields=['inpfile','minsup','minconf'],labels={'inpfile':'Dataset File','minsup':'minsup','minconf':'minconf'}).process()
    if form.accepted:
        response.flash="File Uploaded Successfully"
        f = open('/home/aeon/web2py/applications/MER_Miner/uploads/'+form.vars.inpfile)
        items = set()
        for line in f:
            buff=line.split('\t')
            for i in buff:
                items.add(i)
        itemsCount=len(items)
        if (itemsCount<=11):
            outfile = '/home/aeon/web2py/applications/MER_Miner/merlogs/'+form.vars.inpfile+'.log'
            cmd = 'echo "/home/aeon/web2py/applications/MER_Miner/uploads/'+form.vars.inpfile+'" '+str(form.vars.minsup)+' '+str(form.vars.minconf)+' | mer-apriori > '+outfile
            os.system(cmd)
            optfile = '/home/aeon/web2py/applications/MER_Miner/uploads/mer_rules.txt'
            cmd = 'rm /home/aeon/web2py/applications/MER_Miner/uploads/mer_rules.txt'
            os.system(cmd)
            cmd = 'mv /home/aeon/web2py/applications/MER_Miner/uploads/mer_output_'+str(form.vars.minsup)+'_'+str(form.vars.minconf)+'_verbrose.txt '+optfile
            os.system(cmd)
            redirect(URL('showResults',vars=dict(fname=form.vars.inpfile,ofile=outfile,allrulesfile=optfile)))
        else:
            response.flash="Too many items"
            redirect(URL('tooManyItems'))
    return dict(message=T('Lets mine some mutual exclusion rules'),form=form)


def showResults():
    fname=request.vars.ofile
    allrulesfile=request.vars.allrulesfile
    f = open(fname)
    rules = list()  
    summ = list()  
    hdr = list()
    for line in f:
         m=rule_pat.match(line)
         if (m):
                #print(m.group())
                rules.append(m.group())
         else:
                m=num_rules_pat.match(line)
                if (m):                        
                        nrules=m.group()
                else:
                    m=header_pat.match(line)
                    if (m):
                        hdr=m.group().split('|')
                    else:
                        m=summary_pat.match(line)
                        if (m):
                            summ=m.group().split('|')
    
    return dict(nrules=nrules,rules=rules,summ=summ,hdr=hdr,allrulesfile=allrulesfile)


def getFile():    
    allrulesfile=str(request.vars.allrulesfile)
    from gluon.contenttype import contenttype
    response.headers['Content-Type'] = contenttype('text')
    response.headers['Content-disposition'] = 'attachment; filename=all_mer_rules.txt' 
    import cStringIO
    s = cStringIO.StringIO()
    f = open(allrulesfile)
    s.write(f.read())
    return s.getvalue()


def tooManyItems():
    return dict(errormsg="Too Many Items")


def downloadSampleDS():
    from gluon.contenttype import contenttype
    response.headers['Content-Type'] = contenttype('text')
    response.headers['Content-disposition'] = 'attachment; filename=SampleDataset.txt' 
    import cStringIO
    s = cStringIO.StringIO()
    f = open("/home/aeon/marketBasket.txt")
    s.write(f.read())
    return s.getvalue()


def aboutUs():
    return dict()


def getReport():
    from gluon.contenttype import contenttype
    response.headers['Content-Type'] = contenttype('pdf')
    response.headers['Content-disposition'] = 'attachment; filename=Mutual Exclusion Rule Mining in Transaction Databases' 
    import cStringIO
    s = cStringIO.StringIO()
    f = open("/home/aeon/Mutual_Exclusion_Rules_in_Transaction_Databases.pdf")
    s.write(f.read())
    return s.getvalue()
