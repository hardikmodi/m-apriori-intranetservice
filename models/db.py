db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
db.define_table('datasets',
    Field('inpfile','upload',requires=IS_NOT_EMPTY()),
    Field('minsup','double',requires=IS_FLOAT_IN_RANGE(0,1)),
    Field('minconf','double',requires=IS_FLOAT_IN_RANGE(0,1)),
    Field('resfile',length=512),
    Field('merlogfile',length=512))
